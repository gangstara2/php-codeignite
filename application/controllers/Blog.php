<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: HM
 * Date: 6/28/2016
 * Time: 1:52 PM
 */
class Blog extends CI_Controller
{

    public function index()
    {
        //Load helper url
        $this->load->helper('url');

        //call a fuction in helper url
//        echo site_url();

        //Load blog_model
        $this->load->model('blog_model');

        //call function get_last_ten then insert to array $data
//        $data['list_last_ten'] = $this->blog_model->get_last_ten();

        //Load view
//        $this->load->view('blog-view', $data);
        $this->load->view('blog-view');

        //load a library
        $this->load->library('upload');
    }
}