<?php

class Upload extends CI_Controller
{
    public function index()
    {
        $this->load->view('upload');
    }

    public function do_upload()
    {
        $config['upload_path'] = './userfile/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $data = array('data' => $this->upload->data());

            $this->load->view('upload', $data);
        } else {
            $data = array('error' => $this->upload->display_errors());

            $this->load->view('upload', $data);
        }
    }
}