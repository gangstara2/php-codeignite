<?php

class Blog_model extends CI_Model
{
    public $title;
    public $content;
    public $date;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_last_ten()
    {
//Query
        $query = $this->db->get('tbl_blog', 10);

//get result and return
        return $query->result();
    }

    public function insert()
    {
//get POST data from form
        $this->title = $_POST['title'];
        $this->content = $_POST['content'];
        $this->date = time();

//execute add new
        $this->db->insert('tbl_blog', $this);
    }

    public function update()
    {
//get POST data from form
        $this->title = $_POST['title'];
        $this->content = $_POST['content'];
        $this->date = time();

//execute update
        $this->db->update('tbl_blog', $this, array('id' => $_POST['id']));
    }
}

?>