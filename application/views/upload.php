<?php
if (isset($data)) {
    foreach ($data as $key => $value) {
        echo '<p>' . $key . ': ' . $value . '</p>';
    }
} elseif (isset($error)) {
    echo $error;
}
?>

<form name="upload" method="post" enctype="multipart/form-data" action="upload/do_upload">
    <p>
        <input type="file" name="userfile">
    </p>

    <p>
        <input type="submit" value="Upload">
    </p>
</form>
